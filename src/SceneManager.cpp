#include "SceneManager.h"

#include "StartScene.h"
#include <iostream>



#define MOVE_VELOCITY 0.01f
#define ROTATE_VELOCITY 0.001f

SceneManager* SceneManager::sceneManager;

void SceneManager::init()
{
	// Initialize GLFW
	if (!glfwInit()) exit(EXIT_FAILURE);

	// Select OpenGL 4.3 with a forward compatible core profile.
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_RESIZABLE, FALSE);
	glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, TRUE);

	//glfwWindowHint(GLFW_SAMPLES, 8);

	// Open the window
	std::string title = "Engine";
	window = glfwCreateWindow(WINDOW_WIDTH, WINDOW_HEIGHT, title.c_str(), NULL, NULL);
	if (!window) {
		glfwTerminate();
		exit(EXIT_FAILURE);
	}
	glfwMakeContextCurrent(window);

	// Load the OpenGL functions.
	gl::exts::LoadTest didLoad = gl::sys::LoadFunctions();

	if (!didLoad) {
		//Claen up and abort
		glfwTerminate();
		exit(EXIT_FAILURE);
	}


	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	glfwSetCursorPos(window, 0, 0);


	//glfwSetKeyCallback(window, key_callback);
	//glfwSetScrollCallback(window, scroll_callback);


	gl::ClearColor(0.5f, 0.5f, 0.5f, 1.0f);

	lastCursorPositionX = 0.0;
	lastCursorPositionY = 0.0;
	cursorPositionX = 0.0;
	cursorPositionY = 0.0;

	//gl::Enable(gl::MULTISAMPLE);

	bRunning = true;

	
}

void SceneManager::initVR()
{
	/*ovrGraphicsLuid ovrLuid;
	ovrResult result = ovr_Create(&hmdSession, &ovrLuid);*/

	//if (!OVR_SUCCESS(result)) {
	//	//return false;
	//	//std::cout << "Failed to initiate" << std::endl;
	//}

	//hmdDescription = ovr_GetHmdDesc(hmdSession);
	//hmdResolution = hmdDescription.Resolution;

	//bHMDLoaded = true;

	//// Initialize GLFW
	//if (!glfwInit()) exit(EXIT_FAILURE);

	//// Select OpenGL 4.3 with a forward compatible core profile.
	//glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	//glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	//glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, TRUE);
	//glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	//glfwWindowHint(GLFW_RESIZABLE, FALSE);
	//glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, TRUE);

	//std::string title = "VR Engine";

	//ovrSizei windowSize = { hmdDescription.Resolution.w / 2, hmdDescription.Resolution.h / 2 };

	//window = glfwCreateWindow(windowSize.w, windowSize.h, title.c_str(), glfwGetPrimaryMonitor(), NULL);

	//if (!window) {
	//	glfwTerminate();
	//	exit(EXIT_FAILURE);
	//}

	//glfwMakeContextCurrent(window);
	//glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	//// Load the OpenGL functions.
	//gl::exts::LoadTest didLoad = gl::sys::LoadFunctions();

	//if (!didLoad) {
	//	//Claen up and abort
	//	glfwTerminate();
	//	exit(EXIT_FAILURE);
	//}

	//for (int eye = 0; eye < 2; ++eye) {
	//	ovrSizei textureSize = ovr_GetFovTextureSize(hmdSession, ovrEyeType(eye), hmdDescription.DefaultEyeFov[eye], 1);
	//	eyeTexture[eye] = new TextureBuffer(hmdSession, true, true, textureSize, 1, NULL, 1);
	//	eyeDepthBuffer[eye] = new DepthBuffer(eyeTexture[eye]->GetSize(), 0);

	//	if (!eyeTexture[eye]->TextureSet)
	//	{
	//		VALIDATE(false, "Failed to create texture.");
	//	}
	//}

	//result = ovr_CreateMirrorTextureGL(hmdSession, gl::SRGB8_ALPHA8, windowSize.w, windowSize.h, (ovrTexture**)&mirrorTexture);

	//if (!OVR_SUCCESS(result)) {

	//	VALIDATE(false, "Failed to create mirror texture.");
	//	// reutnr false
	//}

	//gl::Enable(gl::MULTISAMPLE);
	//gl::GenFramebuffers(1, &mirrorFBO);
	//gl::BindFramebuffer(gl::READ_FRAMEBUFFER, mirrorFBO);
	//gl::FramebufferTexture2D(gl::READ_FRAMEBUFFER, gl::COLOR_ATTACHMENT0, gl::TEXTURE_2D, mirrorTexture->OGL.TexId, 0);
	//gl::FramebufferRenderbuffer(gl::READ_FRAMEBUFFER, gl::DEPTH_ATTACHMENT, gl::RENDERBUFFER, 0);
	//gl::BindFramebuffer(gl::READ_FRAMEBUFFER, 0);

	//eyeRenderDesc[0] = ovr_GetRenderDesc(hmdSession, ovrEye_Left, hmdDescription.DefaultEyeFov[0]);
	//eyeRenderDesc[0] = ovr_GetRenderDesc(hmdSession, ovrEye_Right, hmdDescription.DefaultEyeFov[1]);

	//glfwSwapInterval(0);


	//bRunning = true;

}

void SceneManager::changeScene(Scene* sceneIn)
{
	if (!scenes.empty())
	{
		//scenes.back()->Cleanup();
		scenes.pop_back();
	}

	scenes.push_back(sceneIn);
	scenes.back()->initScene(this);



}

void SceneManager::pushScene(Scene* sceneIn)
{
	if (!scenes.empty())
	{
		//scenes.back()->Pause();
	}

	scenes.push_back(sceneIn);
	//scenes.back()->initScene();
}


void SceneManager::handleInput(float t)
{
	//Get the current cursor position
	glfwGetCursorPos(window, &cursorPositionX, &cursorPositionY);

	//See how much the cursor has moved
	float deltaX = (float)(lastCursorPositionX - cursorPositionX);
	float deltaY = (float)(lastCursorPositionY - cursorPositionY);

	// Rotate camera in the x and y axis 
	getActiveSceneCamera()->rotate(deltaX*ROTATE_VELOCITY, deltaY*ROTATE_VELOCITY);

	//scenes.back()->getModel()->rotate(deltaX*ROTATE_VELOCITY, deltaY*ROTATE_VELOCITY, 0.0f);


	// Move camera forward and backwards 
	if (glfwGetKey(window, GLFW_KEY_W))
	{
		getActiveSceneCamera()->move(t * 10, t * 10, t * 10);
	}
	else if (glfwGetKey(window, GLFW_KEY_S))
	{
		getActiveSceneCamera()->move(t*-10, t*-10, t*-10);
	}






	if (glfwGetKey(window, GLFW_KEY_UP)) {

		scenes.back()->getModel()->getModelHandle()->translate(t * 2, 0, t * 2);
	}

	else if (glfwGetKey(window, GLFW_KEY_DOWN)) {

		scenes.back()->getModel()->getModelHandle()->translate(t * -2, 0, t * -2);
	}

	else if (glfwGetKey(window, GLFW_KEY_LEFT)) {
		scenes.back()->getModel()->getModelHandle()->rotate(t * 2, 0, 0);

	}
	else if (glfwGetKey(window, GLFW_KEY_RIGHT)) {
		scenes.back()->getModel()->getModelHandle()->rotate(t * -2, 0, 0);

	}
	else if (glfwGetKey(window, GLFW_KEY_Y)) {
//		scenes.back()->getModel()->rotate(0.0 , 0.0, t * 2);

	}



	//To adjust Roll with MIDDLE mouse button
	if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_MIDDLE))
	{
		std::cout << "Middle mouse button pressed!";
		getActiveSceneCamera()->roll(deltaX*ROTATE_VELOCITY);

	}



	if (glfwGetKey(window, GLFW_KEY_G))
	{
		changeScene(StartScene::Instance());
	}





	//Store the current cursor position
	lastCursorPositionX = cursorPositionX;
	lastCursorPositionY = cursorPositionY;


	//scenes.back()->handleInput(t);
}

void SceneManager::update(float fInterval)
{
	scenes.back()->update(fInterval);
}

void SceneManager::render()
{
	scenes.back()->render();
}

void SceneManager::mainLoop()
{
	float fLastTime = (float)glfwGetTime();
	
		while (!glfwWindowShouldClose(window) && !glfwGetKey(window, GLFW_KEY_ESCAPE)) {
	
	
			float fCurrentTime = (float)glfwGetTime();
			float fInterval = fCurrentTime - fLastTime;
	
			handleInput(fInterval);

			update(fInterval);

			render();
	
			glfwSwapBuffers(window);
			glfwPollEvents();
	
			fLastTime = fCurrentTime;
		}
}

void SceneManager::mainLoopVR()
{
	float fLastTime = (float)glfwGetTime();

	while (!glfwWindowShouldClose(window) && !glfwGetKey(window, GLFW_KEY_ESCAPE)) {


		float fCurrentTime = (float)glfwGetTime();
		float fInterval = fCurrentTime - fLastTime;

		handleInput(fInterval);

		update(fInterval);

		render();




		glfwSwapBuffers(window);
		glfwPollEvents();

		fLastTime = fCurrentTime;
	}
}

void SceneManager::clean()
{
	// Cleanup all scenes
	while (!scenes.empty()) {
		//scenes.back()->Cleanup(); // FINISH CLEAN UP FUNCTION 
		scenes.pop_back();
	}

	printf("Cleaning scene manager...");

	glfwTerminate();
}

void SceneManager::resizeGL(Camera* camera, int w, int h) 
{
	scenes.back()->resize(w, h);
}

//void SceneManager::key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
//{
//
//	if (key == GLFW_KEY_W)
//	{
//
//		
//		getActiveSceneCamera()->
//	}
//}

//void SceneManager::scroll_callback(GLFWwindow *window, double x, double y)
//{
//	getActiveSceneCamera()->zoom((float)y*0.5f);
//}

Camera* SceneManager::getActiveSceneCamera()
{
	return scenes.back()->getCamera();
}
