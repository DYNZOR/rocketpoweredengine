#include "stdafx.h"
#include "gl_core_4_3.hpp"
#include "SceneManager.h"

#include "scene.h"
#include "MainScene.h"


//class Component {
//public : 
//
//};
//
//
//class Drawable : public Component {
//
//};
//
//class 

///////////////////////////////////////////////////////
///////  Main  ////////////////////////////////////////
///////////////////////////////////////////////////////
int _tmain(int argc, _TCHAR* argv[])
{

	


	SceneManager* manager = SceneManager::SceneManagerInstance();

	manager->init();

	manager->changeScene(MainScene::Instance() );

	manager->resizeGL(manager->getActiveSceneCamera(), 1200, 700);

	//while ( manager->running() ) {

		manager->mainLoop();
	//}

	manager->clean();
	//delete scene;


	// Exit program
	exit(EXIT_SUCCESS);
}

