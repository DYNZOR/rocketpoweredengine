#include "SceneModel.h"

SceneModel::SceneModel(std::string sName, const glm::vec3 vPos, glm::quat qRotation, glm::vec3 vScale, const char* kcProgName)
{

	//graphics = assetLoader::getInstance()->getModelComponent(model);

	pModel = AssetManager::AssetManagerInstance()->getModel(sName);

		//ModelManager::ModelManagerInstance()->getModelByName(sName);
	// std::make_shared<Model>(Model(sName));


	//std::make_shared<Model>(sName);
	//pModel->setDirectory(sPathIn);
	//pModel->loadModel();

	pModel->setActiveShader(AssetManager::AssetManagerInstance()->getShaderProgram(kcProgName));

	pModel->setPosition(vPos);
	pModel->setRotation(qRotation);

	pModel->setScale(vScale);

}

SceneModel::~SceneModel()
{

}

void SceneModel::update(float dt)
{

}

void SceneModel::render()
{
	pModel->render();

//	graphics->update(*this);
}