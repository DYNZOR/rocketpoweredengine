#include "stdafx.h"

#include "MainScene.h"
#include <cstdio>
#include <cstdlib>

#include <iostream>
#include <fstream>
using std::ifstream;
#include <sstream>
#include <string>
using std::string;

#include "glm\glm.hpp"
#include "glm\gtc\matrix_transform.hpp"
#include "glm\gtx\transform.hpp"
#include "glm\gtc\type_ptr.hpp"
using glm::vec3;

//MainScene::MainScene() { }

MainScene MainScene::s_MainScene;

void MainScene::initScene(SceneManager* manager)
{

	camera = Camera();

	gl::Enable(gl::DEPTH_TEST);

	loadSceneFromXML("resources/xml/gameLayout.xml");
	

	cubeMap = std::make_shared<CubeMap>("moonwaw");
	cubeMap->setActiveShader(AssetManager::AssetManagerInstance()->getShaderProgram("skybox"));
	cubeMap->loadCubeMap();


	setLightParams();

	// Model data to be moved into class
	fAngle = 0.0f;


	//plane = new Model("resources/models/plane/untitled.fbx");

	//suit = new Model("resources/models/nanosuit/nanosuit.obj");
	//suit->setPosition(0.0f, -1.5f, 0.0f);
	//suit->setRotation(0.0f, 229.4f, 0.0f);
	//suit->setScale(glm::vec3(0.3, 0.3, 0.3));


	//dragon = new Model("resources/models/dragon/smaug.obj");

    //spaceStation = new Model("resources/models/intercepter/Arc170.obj");


}

void MainScene::setLightParams()
{
	AssetManager::AssetManagerInstance()->getShaderProgram("basic")->setUniform("viewPosition", camera.getPosition());

	vec3 worldLight = vec3(0.0f, 10.0, 5.0);
	vec3 ambientIntensity = vec3(0.5f, 0.5f, 0.5f);
	vec3 diffuseIntensity = vec3(1.0f, 1.0f, 1.0f);
	vec3 specularIntensity = vec3(1.0f, 1.0f, 1.0f);


	AssetManager::AssetManagerInstance()->getShaderProgram("basic")->setUniform("light.Position", worldLight);
	AssetManager::AssetManagerInstance()->getShaderProgram("basic")->setUniform("light.La", ambientIntensity);
	AssetManager::AssetManagerInstance()->getShaderProgram("basic")->setUniform("light.Ld", diffuseIntensity);
	AssetManager::AssetManagerInstance()->getShaderProgram("basic")->setUniform("light.Ls", specularIntensity);

	AssetManager::AssetManagerInstance()->getShaderProgram("basic")->setUniform("light.constant", 1.0f);
	AssetManager::AssetManagerInstance()->getShaderProgram("basic")->setUniform("light.linear", 0.009f);
	AssetManager::AssetManagerInstance()->getShaderProgram("basic")->setUniform("light.quadratic", 0.0032f);
}

void MainScene::setUpMatrices(glm::mat4 matrix)
{
	basicShaderProgram->setUniform("mModelMatrix", matrix);
	basicShaderProgram->setUniform("mViewMatrix", camera.getViewMatrix());
	basicShaderProgram->setUniform("mProjectionMatrix", camera.getProjectionMatrix());

}

void MainScene::update(float t)
{
	fAngle += (10.0f * t);
}

void MainScene::render()
{
	gl::Clear(gl::COLOR_BUFFER_BIT | gl::DEPTH_BUFFER_BIT);

	// Render cube map
	AssetManager::AssetManagerInstance()->getShaderManager()->setActiveShader("skybox");

	AssetManager::AssetManagerInstance()->getShaderProgram("skybox")->setUniform("mViewMatrix", camera.getViewMatrix());
	AssetManager::AssetManagerInstance()->getShaderProgram("skybox")->setUniform("mProjectionMatrix", camera.getProjectionMatrix());
	cubeMap->render();

	// Render scene models 
	AssetManager::AssetManagerInstance()->getShaderManager()->setActiveShader("basic");

	AssetManager::AssetManagerInstance()->getShaderProgram("basic")->setUniform("mViewMatrix", camera.getViewMatrix());
	AssetManager::AssetManagerInstance()->getShaderProgram("basic")->setUniform("mProjectionMatrix", camera.getProjectionMatrix());

	for (modelsIt = models.begin(); modelsIt != models.end(); ++modelsIt)
	{
		setLightParams();
		
		modelsIt->get()->render();
	}
}

void MainScene::handleInput(float t)
{
	//fAngle += (10.0f * t);
}

void MainScene::resize(int w, int h)
{
	gl::Viewport(0, 0, w, h);
	width = w;
	height = h;
	camera.setAspectRatio((float) w / h);
}

void MainScene::dispose()
{

}

void MainScene::loadSceneFromXML(const std::string filePath)
{
	tinyxml2::XMLDocument xmlDoc;
	xmlDoc.LoadFile((filePath).c_str());

	// Load texture manager 
	//TextureLoader texloader;
	const char * attributeText = nullptr;

	tinyxml2::XMLNode * pRoot = xmlDoc.FirstChildElement("Root");

	if (pRoot == nullptr) {
		std::cout << "No file found" << std::endl;
	}


	tinyxml2::XMLElement * pElement = pRoot->FirstChildElement("SceneObjects");

	if (pElement == nullptr) {
		std::cout << "pElement not found." << std::endl;
	}

	tinyxml2::XMLElement * pListElement = pElement->FirstChildElement("Model");

	while (pListElement != nullptr)
	{

		glm::vec3 tempPosition;
		glm::quat tempRotation;
		glm::vec3 tempScale;
		//float tempScale;

		float outFloat;

		tinyxml2::XMLElement * pInsideListElement = pListElement->FirstChildElement("name");

		std::string nameOfElement = std::string(pInsideListElement->GetText());

		pInsideListElement = pInsideListElement->NextSiblingElement("positionX");
		pInsideListElement->QueryFloatText(&outFloat);
		tempPosition.x = outFloat;

		pInsideListElement = pInsideListElement->NextSiblingElement("positionY");
		pInsideListElement->QueryFloatText(&outFloat);
		tempPosition.y = outFloat;

		pInsideListElement = pInsideListElement->NextSiblingElement("positionZ");
		pInsideListElement->QueryFloatText(&outFloat);
		tempPosition.z = outFloat;


		pInsideListElement = pInsideListElement->NextSiblingElement("rotationAngle");
		pInsideListElement->QueryFloatText(&outFloat);
		tempRotation.w = outFloat;

		pInsideListElement = pInsideListElement->NextSiblingElement("rotationXAxis");
		pInsideListElement->QueryFloatText(&outFloat);
		tempRotation.x = outFloat;

		pInsideListElement = pInsideListElement->NextSiblingElement("rotationYAxis");
		pInsideListElement->QueryFloatText(&outFloat);
		tempRotation.y = outFloat;

		pInsideListElement = pInsideListElement->NextSiblingElement("rotationZAxis");
		pInsideListElement->QueryFloatText(&outFloat);
		tempRotation.z = outFloat;


		pInsideListElement = pInsideListElement->NextSiblingElement("scaleX");
		pInsideListElement->QueryFloatText(&outFloat);
		tempScale.x = outFloat;

		pInsideListElement = pInsideListElement->NextSiblingElement("scaleY");
		pInsideListElement->QueryFloatText(&outFloat);
		tempScale.y = outFloat;

		pInsideListElement = pInsideListElement->NextSiblingElement("scaleZ");
		pInsideListElement->QueryFloatText(&outFloat);
		tempScale.z = outFloat;


		pInsideListElement = pInsideListElement->NextSiblingElement("shaderProgram");

		std::string programName = std::string(pInsideListElement->GetText());


		models.push_back(factory.createModel(nameOfElement, tempPosition, tempRotation, tempScale, programName.c_str()));

		pListElement = pListElement->NextSiblingElement("Model");

		//pRootElement = pRootElement->NextSiblingElement();
	}
}

