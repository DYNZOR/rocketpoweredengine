#include "stdafx.h"
#include <cstdio>
#include <cstdlib>

#include "StartScene.h"

#include <string>
using std::string;

using glm::vec3;

//MainScene::MainScene() { }

StartScene StartScene::s_StartScene;

void StartScene::initScene(SceneManager* manager)
{

	camera = Camera();

	gl::Enable(gl::DEPTH_TEST);

	cubeMapShaderProgram = std::make_shared<ShaderProgram>();

	cubeMapShaderProgram->loadShaderFromFile("resources/shaders/skybox.vert", gl::VERTEX_SHADER);
	cubeMapShaderProgram->loadShaderFromFile("resources/shaders/skybox.frag", gl::FRAGMENT_SHADER);
	cubeMapShaderProgram->linkProgram();
	cubeMapShaderProgram->useProgram();

	cubeMap = std::make_shared<CubeMap>("moonwaw");
	cubeMap->loadCubeMap();

	setLightParams();
}

void StartScene::setLightParams()
{

}

void StartScene::setUpMatrices(glm::mat4 matrix)
{
	

}

void StartScene::update(float t)
{

	//camera.rotate(5 * t, 0.0);
	//	spaceStation->move(0.0f, 0.0f, fAngle);


}

void StartScene::render()
{
	gl::Clear(gl::COLOR_BUFFER_BIT | gl::DEPTH_BUFFER_BIT);

	// Render cube map

	glm::mat4 cubeMapModelMat = glm::mat4(1.0f);
	cubeMapShaderProgram->setUniform("mModelMatrix", cubeMapModelMat);
	cubeMapShaderProgram->setUniform("mViewMatrix", camera.getViewMatrix());
	cubeMapShaderProgram->setUniform("mProjectionMatrix", camera.getProjectionMatrix());
	cubeMap->render();

}

void StartScene::handleInput(float t)
{
	//fAngle += (10.0f * t);
}

void StartScene::resize(int w, int h)
{
	gl::Viewport(0, 0, w, h);
	width = w;
	height = h;
	camera.setAspectRatio((float)w / h);
}

void StartScene::dispose()
{

}

