#include "SoundManager.h"
#include <exception>


soundManager::soundManager()
{
	system = NULL;
	lastpos = { 0.0f, 0.0f, 0.0f };
	for (int channelIndex = 0; channelIndex < numberOfChannels; channelIndex++)
		channelArray[channelIndex].Clear();
}

soundManager* soundManager::getInstance(){
	static soundManager *theInstance = new soundManager();
	return theInstance;}soundManager::~soundManager()
{
	if (system)
	{
		system->release();
	}
}

void soundManager::FMODisOK(FMOD_RESULT result)
{
	if (result != FMOD_OK)
	{
		throw ("FMOD error " + std::to_string(result) + ": " + FMOD_ErrorString(result) + placeOfError);
	}
}

void soundManager::Initialize(void)
{
	placeOfError = "soundManager::Initialize";
	// Create the main system object.
	unsigned int version;
	FMOD_RESULT result;
	result = FMOD::System_Create(&system);
	FMODisOK(result);
		
	result = system->getVersion(&version);
	FMODisOK(result);

	if (version < FMOD_VERSION)
	{
		throw ("Error! You are using an old version of FMOD " + std::to_string(version) + ". This program requires " + std::to_string(FMOD_VERSION));
	}


	result = system->init(numberOfChannels/* the amount of channels possibly change*/, FMOD_INIT_NORMAL, 0);    // Initialize FMOD.
	if (result != FMOD_OK)
	FMODisOK(result);

	system->set3DSettings(1.0, 1.0, 1.0);//set the deafualt settings 
	FMODisOK(result);


}

void soundManager::update(glm::vec3 pos, glm::quat Orentaion/*can be changed to the cameras up and forward vector*/, float dt)
{
	placeOfError = "soundManager::update";
	FMOD_RESULT result;
	FMOD_VECTOR position = { 0.0f, 0.0f, 0.0f };
	FMOD_VECTOR forward = { 0.0f, 0.0f, 0.0f };
	FMOD_VECTOR up = { 0.0f, 0.0f, 0.0f };
	FMOD_VECTOR vel = { 0.0f, 0.0f, 0.0f };

	glm::mat4 _view = glm::mat4_cast(Orentaion);

	glm::vec3 forwardTemp = glm::vec3(_view[2][0], _view[2][1], _view[2][2]);
	glm::normalize(forwardTemp);

	forward = glmVectorToFmodVector(forwardTemp);

	glm::vec3 upTemp = glm::vec3(_view[1][0], _view[1][1], _view[1][2]);
	glm::normalize(upTemp);

	up = glmVectorToFmodVector(upTemp);
	position = glmVectorToFmodVector(pos);
	vel.x = (position.x - lastpos.x) * dt;
	vel.y = (position.y - lastpos.y) * dt;
	vel.z = (position.z - lastpos.z) * dt;

	lastpos = position;

	result = system->set3DListenerAttributes(0, &position, &vel, &forward, &up);
	FMODisOK(result);
	system->update();
}

FMOD_VECTOR soundManager::glmVectorToFmodVector(const glm::vec3 vector)
{
	FMOD_VECTOR vec;

	vec.x = vector.x;
	vec.y = vector.y;
	vec.z = vector.z;

	return vec;
}

int soundManager::Create3DSound(std::string &filePath)
{
	placeOfError = "soundManager::Create3DSound";
	FMOD_RESULT    result;
	FMOD::Sound *  sound;
	soundInstance soundToBeCreated;
	int soundIndex;
	if (sound3D.find(filePath) == sound3D.end())
		{
		//no sound found
		soundToBeCreated.fileName = filePath;
		soundToBeCreated.soundType = sound_3D;
		result = system->createSound(filePath.c_str(), FMOD_3D, 0, &sound);
		FMODisOK(result);
		soundToBeCreated.fmodSound = sound;
		sound3D[filePath] = soundToBeCreated;
	}
		//found sound
		return sound_3D;


}
int soundManager::Create3DLoopedSound(std::string &filePath)
{
	placeOfError = "soundManager::Create3DLoopedSound";
	FMOD_RESULT    result;
	FMOD::Sound *  sound;
	soundInstance soundToBeCreated;
	int soundIndex;
	if (sound3DLooped.find(filePath) == sound3DLooped.end())
	{
		//no sound found
		soundToBeCreated.fileName = filePath;
		soundToBeCreated.soundType = sound_3D;
		result = system->createSound(filePath.c_str(), FMOD_3D, 0, &sound);
		FMODisOK(result);
		result = sound->setMode(FMOD_LOOP_NORMAL);
		FMODisOK(result);
		soundToBeCreated.fmodSound = sound;
		sound3DLooped[filePath] = soundToBeCreated;
	}

	//found sound
	return sound_3D_looped;

}
int soundManager::Create2DSound(std::string &filePath)
{
	placeOfError = "soundManager::Create2DSound";
	FMOD_RESULT    result;
	FMOD::Sound *  sound;
	soundInstance soundToBeCreated;
	int soundIndex;
	if (sound2D.find(filePath) == sound2D.end())
	{
		//no sound found
		soundToBeCreated.fileName = filePath;
		soundToBeCreated.soundType = sound_3D;
		result = system->createSound(filePath.c_str(), FMOD_2D, 0, &sound);
		FMODisOK(result);
		soundToBeCreated.fmodSound = sound;
		sound2D[filePath] = soundToBeCreated;
	}

	//found sound
	return sound_2D;

}
int soundManager::Create2DLoopedStream(std::string &filePath)
{
	placeOfError = "soundManager::Create2DSound";
	FMOD_RESULT    result;
	FMOD::Sound *  sound;
	soundInstance soundToBeCreated;
	int soundIndex;
	if (stream2DLooped.find(filePath) == stream2DLooped.end())
	{
		//no sound found
		soundToBeCreated.fileName = filePath;
		soundToBeCreated.soundType = sound_3D;
		result = system->createStream(filePath.c_str(), FMOD_2D, 0, &sound);
		FMODisOK(result);
		result = sound->setMode(FMOD_LOOP_NORMAL);
		FMODisOK(result);
		soundToBeCreated.fmodSound = sound;
		stream2DLooped[filePath] = soundToBeCreated;
	}

	//found sound
	return sound_2D_looped;
}

void soundManager::playSound(std::string &filePath, int objectID, int soundType, int *channelIndex)
{
	placeOfError = "soundManager::playSound";
	sounds type = (sounds)soundType;
	FMOD::Channel *channel;
	FMOD_RESULT result;
	soundInstance soundint;
	int channelIndexTemp;
	switch(type)
	{
		case(sound_3D):
		{
			if (!sound3D.count(filePath))
			{
				return;
			}
			soundint = sound3D.at(filePath);
			break;
		}

		case(sound_3D_looped) :
		{
			if (!sound3DLooped.count(filePath))
			{
				return;
			}
			soundint = sound3DLooped.at(filePath);
			break;
		}

		case(sound_2D) :
		{
			if (!sound2D.count(filePath))
			{
				return;
			}
			soundint = sound2D.at(filePath);
			break;
		}

		case(sound_2D_looped) :
		{
			if (!stream2DLooped.count(filePath))
			{
				return;
			}
			soundint = stream2DLooped.at(filePath);
			break;
		}
	}

	if (*channelIndex != NULL) //potential error
	{
		channelIndexTemp = *channelIndex;
		result = system->getChannel(channelIndexTemp, &channel);
		FMODisOK(result);
		bool isPlaying;
		result = channel->isPlaying(&isPlaying);
		if ((result == FMOD_OK) && (isPlaying == true) && (channelArray[channelIndexTemp].objectID == objectID))
		{
			return;//already playing
		}
	}
	else
	{
		channelIndexTemp = 0;
	}

	result = system->playSound(soundint.fmodSound,0, false, &channel);
	if (result == FMOD_OK)
	{
		std::cout << "SoundManager::PlaySound could not play sound  FMOD Error:" << FMOD_ErrorString(result);
		*channelIndex = NULL;
		return;
	}

	channel->getIndex(&channelIndexTemp);
	//channelArray[channelIndexTemp].sceneNode = pointer to a position of a 3D sound object;
	/*if (soundNode)
	{
		channelArray[channelIndexTemp].prevPosition = soundNode->getWorldPosition();

		initialPosition.x = soundNode->getWorldPosition().x;
		initialPosition.y = soundNode->getWorldPosition().y;
		initialPosition.z = soundNode->getWorldPosition().z;
		channel->set3DAttributes(&initialPosition, NULL);
	}*/
	result = channel->setVolume(1.0);
	// This is where the sound really starts.
	result = channel->setPaused(false);

	*channelIndex = channelIndexTemp;
}

FMOD::Channel *soundManager::GetSoundChannel(int channelIndex)
{
	if (channelIndex > 0 && channelIndex < numberOfChannels)
	{
		FMOD::Channel *soundChannel;
		system->getChannel(channelIndex, &soundChannel);
		return soundChannel;
	}
		return NULL;
}

void soundManager::Set3DMinMaxDistance(int channelIndex, float minDistance, float maxDistance)
{
	FMOD_RESULT    result;
	FMOD::Channel *channel;

	if (channelIndex == invalid)
		return;

	result = system->getChannel(channelIndex, &channel);
	if (result == FMOD_OK)
		channel->set3DMinMaxDistance(minDistance, maxDistance);
}

void soundManager::StopAllSounds()
{
	int            channelIndex;
	FMOD_RESULT    result;
	FMOD::Channel *nextChannel;

	for (channelIndex = 0; channelIndex < numberOfChannels; channelIndex++)
	{
		result = system->getChannel(channelIndex, &nextChannel);
		if ((result == FMOD_OK) && (nextChannel != NULL))
			nextChannel->stop();
		channelArray[channelIndex].Clear();
	}
}

void soundManager::StopSound(int *channelIndex)
{
	if (*channelIndex > 0 && *channelIndex < numberOfChannels)
	{
		FMOD::Channel *soundChannel;


		system->getChannel(*channelIndex, &soundChannel);
		soundChannel->stop();

		channelArray[*channelIndex].Clear();
		*channelIndex = invalid;
	}
}