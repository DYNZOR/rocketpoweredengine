#ifndef SOUNDMANAGER_H
#define SOUNDMANAGER_H

#include <iostream>
#include <fstream>
#include <string>
#include "fmod.hpp"
#include "fmod_errors.h"
#include <map>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform2.hpp>
#include <glm/gtx/quaternion.hpp>
#include "Channel.h"
enum sounds { sound_3D, sound_3D_looped, sound_2D_looped, sound_2D };

struct soundInstance
{
	std::string fileName;
	FMOD::Sound * fmodSound;
	sounds soundType;
};

const int numberOfChannels = 200;
const int invalid = -1;
class soundManager
{
private:
	FMOD::System *system;
	std::string placeOfError;
	FMOD_VECTOR lastpos;
	void FMODisOK(FMOD_RESULT result);
	FMOD_VECTOR glmVectorToFmodVector(const glm::vec3 vector);
	std::map<std::string, soundInstance> sound3D;
	std::map<std::string, soundInstance> sound3DLooped;
	std::map<std::string, soundInstance> sound2D;
	std::map<std::string, soundInstance> stream2DLooped;
	Channel channelArray[numberOfChannels];
public:	static soundManager* getInstance();	soundManager();	~soundManager();
	void Initialize(void);

	int Create3DSound(std::string &filePath);         // single-shot 3D sound.  returns soundType
	int Create3DLoopedSound(std::string &filePath);   // looping 3D sound.  returns soundType
	
	int Create2DSound(std::string &filePath);
	int Create2DLoopedStream(std::string &filePath);  // looping 2D stream.  returns soundType
	
	void playSound(std::string &filePath, int objectID, int soundType, int *channelIndex);
	void StopAllSounds();
	void StopSound(int *channelIndex);
	void Set3DMinMaxDistance(int channelIndex, float minDistance, float maxDistance);
	float GetSoundLength(std::string &filePath, int soundType = invalid);
	FMOD::Channel *GetSoundChannel(int channelIndex);
	//updats the listener needs to be done every frame
	void update(const glm::vec3 position,const glm::quat Orentaion,const float dt);
	
};

#endif SOUNDMANAGER_H