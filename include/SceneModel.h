/** \file gameObject.h
* Header file for the gameObject class.
*/

#ifndef SCENEMODEL_H
#define SCENEMODEL_H

#include "SceneObject.h"
#include "AssetManager.h"

class SceneModel : public SceneObject
{
protected: 

	std::shared_ptr<Model> pModel;

public:

	/** sets all the pointers to null. */
	SceneModel(const std::string sNameIn, const glm::vec3 vPos, glm::quat qRotation, glm::vec3 vScale, const char* kcProgName);
	/**Resets the the pointer objects. */
	~SceneModel();

	/** A pointer to the graphics component */

	/** updates the object before drawing
	\param dt the delta time to update
	*/
	virtual void update(float dt) override;
	/**Draw the scene.*/
	virtual void render() override;

	std::shared_ptr<Model> getModelHandle() { return pModel;  }


};
#endif  GAMEOBJECT_H