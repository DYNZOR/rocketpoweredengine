
/**
@file Object.h
*/

#ifndef OBJECT_H
#define OBJECT_H

#include <glm\glm.hpp>
#include <memory>
#include <vector>

/*! \class Greenhorn::Graphics::Object
\brief A class which represents an object in world space
*/
class Object
{

protected:

	//std::shared_ptr<Object> m_Parent;
	//std::vector<std::shared_ptr<Object>> m_aChildren;

	//glm::vec3 vPosition; //!< The position of the object in world space

public:
	Object(); //!< Default constructor
	~Object(); //!< Default deconstructor
	
	//void setPosition(const float X, const float Y, const float Z); //!< Sets the position of the object 
	//void setPosition(const glm::vec3 kPos); //!< Sets the position of the object

	//void translate(const float kX, const float kY, const float kZ); //!< Translates the object using 3 floats
	//void translate(const glm::vec3 kTranslate); //!< Translates the object using a glm::vec3

	//virtual glm::vec3 getPosition() const {
	//	//std::shared_ptr<Object> parent = m_Parent.lock();
	//	//if (parent)
	//	//{
	//	//	return m_Position + parent->GetPosition();
	//	//}
	//	//else
	//	//{
	//		return vPosition;
	//	//}
	//} //!< Returns the position of the object

	//void SetParent(std::shared_ptr<Object> parent);
	//void AddChild(std::shared_ptr<Object> newChild);

};

#endif