/** \file AssetManager.h
* Header file for the assetLoader class.
*/

#ifndef ASSETMANAGER_H
#define ASSETMANAGER_H

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <map>
#include <memory>

#include "ModelManager.h"
#include "ShaderManager.h"

//! The assetLoader Class
/*!
This class provides access to the managers for shaders, textures, models and audio. 
*/
class AssetManager
{
private:

	static AssetManager* pAssetManager;

	/** sets all the bools to false for the input */
	AssetManager();
	~AssetManager();

	// Texture manager instance 

	ModelManager* pModelManager; //!< Model manager instance 

	ShaderManager* pShaderManager; //!< Shader manager instance

	// Audio manager instance 


	/** a map of models to be used at a later basis */
	//std::map<std::string, std::shared_ptr<modelGraphicsComponent>> models;
	/** a map of textures to be used at a later basis */
	//std::map<std::string, GLuint> texture;
	/** a map of shaders to be used at a later basis */
	//std::map<std::string, GLuint> shaders;

public:
	 
	static AssetManager* AssetManagerInstance();
	/**
	creates a static instance of the object
	*/
	//static AssetManager* AssetManagerInstance();

	/** gets the program of the current shader in use */
	//GLuint currentshader;
	/** gets the name of the current shader in use */
	//std::string currentShaderName;
	/** gets the curren tCamera in use */
	//std::shared_ptr<camera> currentCamera;

	std::shared_ptr<Model> getModel(std::string sModelName) { return pModelManager->getModelByName(sModelName) ;  }
	std::shared_ptr<ShaderProgram> getShaderProgram(const char* kcProgName) { return pShaderManager->getShaderProgram(kcProgName); }


	// Texture manager getter
	// Shader manager getter 
	ShaderManager* getShaderManager() { return pShaderManager; }


	/**
	loads a shader into a map
	\param name the name the shader is to be mapped to
	\param porg the program the name relates to
	*/
	//void loadShader(std::string name, GLuint porg);
	/**
	loads a model into a map
	\param name the name the model is to be mapped to
	\param porg the model component the name relates to
	*/
	//void loadSingleModel(std::string name, std::shared_ptr<modelGraphicsComponent> model);
	/**
	loads a texture into a map
	\param name the name the texture is to be mapped to
	\param porg the texture the name relates to
	*/
//	void loadTextureMap(std::string name, GLuint texture);
	/**
	returns the texture the name is related too
	\param name the name of the texture to get
	*/
//	GLuint getTexture(std::string name);

	/**
	sets the shader name to the current shader
	\param name the name of the shader to set as active
	*/
//	void setActiveShader(const std::string& name);
	/**
	gets the ModelComponent in relation to its name
	\param name the name of the model to be retuned
	*/
//	std::shared_ptr<modelGraphicsComponent> getModelComponent(std::string name);
};

#endif 